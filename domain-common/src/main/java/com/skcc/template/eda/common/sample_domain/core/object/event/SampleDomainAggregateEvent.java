package com.skcc.template.eda.common.sample_domain.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class SampleDomainAggregateEvent {

  private String id;
  private String status;
  private String sampleData1;
  private String sampleData2;
}