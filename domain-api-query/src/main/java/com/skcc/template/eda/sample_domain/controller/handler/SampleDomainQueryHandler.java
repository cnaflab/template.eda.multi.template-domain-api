package com.skcc.template.eda.sample_domain.controller.handler;

import java.util.List;

import com.skcc.template.eda.sample_domain.core.application.service.ISampleDomainQueryService;
import com.skcc.template.eda.common.sample_domain.core.object.query.SampleDomainQueryMessage;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainQueryResponseDTO;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainRequestDTO;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor
public class SampleDomainQueryHandler {

    private final ISampleDomainQueryService sampleDomainQueryService;

    @QueryHandler
    public List<SampleDomainQueryResponseDTO> on(SampleDomainQueryMessage query){
        log.debug("[Query Handler Called] SampleDomainQueryMessage");
        log.debug("handling {}", query);

        SampleDomainRequestDTO sampleDomainRequestDTO = new SampleDomainRequestDTO(
            query.getId()
            , query.getDomainId() );

        return sampleDomainQueryService.getDomainQueryData(sampleDomainRequestDTO);
    }
}