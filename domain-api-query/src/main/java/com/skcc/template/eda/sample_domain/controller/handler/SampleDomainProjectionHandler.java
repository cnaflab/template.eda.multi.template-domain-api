package com.skcc.template.eda.sample_domain.controller.handler;

import java.time.Instant;

import com.skcc.template.eda.common.sample_domain.core.object.event.SampleDomainAggregateEvent;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainProjctionDTO;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleDomainQueryService;

import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor
public class SampleDomainProjectionHandler {

    private final ISampleDomainQueryService sampleDomainQueryService;

    @EventHandler
    protected void on(SampleDomainAggregateEvent event, @Timestamp Instant instant){
        log.debug("[Projection Handler Called] SampleDomainAggregateEvent");
        log.debug("projecting {} , timestamp : {}", event, instant.toString());

        SampleDomainProjctionDTO sampleDomainProjctionDTO = SampleDomainProjctionDTO.builder()
                                                                                .domainId(event.getId())
                                                                                .sampleData1(event.getSampleData1())
                                                                                .sampleData2(event.getSampleData2())
                                                                                .status(event.getStatus())
                                                                                .build();

        sampleDomainQueryService.putSampleDataAggregateStatus(sampleDomainProjctionDTO);
    }
}