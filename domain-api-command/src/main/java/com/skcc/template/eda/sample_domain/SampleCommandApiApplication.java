package com.skcc.template.eda.sample_domain;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SampleCommandApiApplication {

	private static final String PROPERTIES =
									"spring.config.location="
									+ "classpath:/config/application/";
		
	public static void main(String[] args) {
		//SpringApplication.run(SampleCommandApiApplication.class, args);
		new SpringApplicationBuilder(SampleCommandApiApplication.class)
            .properties(PROPERTIES)
            .run(args);
	}

}
