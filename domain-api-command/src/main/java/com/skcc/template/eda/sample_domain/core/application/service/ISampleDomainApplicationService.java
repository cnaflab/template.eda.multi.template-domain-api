package com.skcc.template.eda.sample_domain.core.application.service;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.PlaceSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.SuccessSampleDomainAggregateCommand;

public interface ISampleDomainApplicationService {
    
    public void createAggregate(CreateSampleDomainAggregateCommand cmd) throws Exception;

    public void placeAggregate(PlaceSampleDomainAggregateCommand cmd) throws Exception;

    public void successAggregate(SuccessSampleDomainAggregateCommand cmd) throws Exception;

    public void failAggregate(FailSampleDomainAggregateCommand cmd) throws Exception;
}
