package com.skcc.template.eda.sample_domain.core.domain.entity;

import com.skcc.template.eda.common.exception.InvalidStatusForCommandException;
import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.event.SampleDomainAggregateEvent;
import com.skcc.template.eda.sample_domain.core.application.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.PlaceSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.SuccessSampleDomainAggregateCommand;

import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ToString
@Aggregate
public class SampleDomainAggregate {

    @AggregateIdentifier
    private String id;
    private SampleDomainStatusVO status;
    private String sampleData1;
    private String sampleData2;

    protected SampleDomainAggregate() {}

    public SampleDomainAggregate(CreateSampleDomainAggregateCommand cmd) {
        log.debug("[Aggregate Called] SampleDomainAggregate[CreateSampleDomainAggregateCommand][" + this.toString() + "]");
    }

    public void doSomeAggregateBusinessLogic(){
        log.debug("[Aggregate Called][Logic] doSomeAggregateBusinessLogic[" + this.toString() + "]");
    }



    @EventSourcingHandler
    protected void eventSrouce(SampleDomainAggregateEvent event){
        this.id = event.getId();
        this.status = SampleDomainStatusVO.valueOf(event.getStatus());

        log.debug("[Aggregate Called][EventSourcingHandler] SampleDomainAggregateEvent[" + this.toString() + "]");
    }

	public Object handle(CreateSampleDomainAggregateCommand cmd) {
        log.debug("[Aggregate Called][Command Handle] handle[CreateSampleDomainAggregateCommand]");

        // Validate the last Aggregate Status by Parameter Command
        if(this.status != null) throw new InvalidStatusForCommandException(cmd, this.status);

        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.SUBMIT;
        this.sampleData1 = cmd.getSampleData1();
        this.sampleData2 = cmd.getSampleData2();

        AggregateLifecycle.apply(new SampleDomainAggregateEvent( 
                                            cmd.getId()
                                            , this.status.name()
                                            , cmd.getSampleData1()
                                            , cmd.getSampleData2() ));
		return this;
    }
    
    public Object handle(PlaceSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Aggregate Called][Command Handle] handle[PlaceSampleDomainAggregateCommand]");

        // Validate the last Aggregate Status by Parameter Command
        if(this.status != SampleDomainStatusVO.SUBMIT) throw new InvalidStatusForCommandException(cmd, this.status);

        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.PLACED;
        this.sampleData1 = cmd.getSampleData1();
        this.sampleData2 = cmd.getSampleData2();

        AggregateLifecycle.apply(new SampleDomainAggregateEvent( 
                                            cmd.getId()
                                            , this.status.name()
                                            , cmd.getSampleData1()
                                            , cmd.getSampleData2() ));
        return this;
    }
    
    public Object handle(SuccessSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Aggregate Called][Command Handle] handle[SuccessSampleDomainAggregateCommand]");

        // Validate the last Aggregate Status by Parameter Command
        if(this.status != SampleDomainStatusVO.PLACED) throw new InvalidStatusForCommandException(cmd, this.status);

        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.SUCCESS;
        this.sampleData1 = cmd.getSampleData1();
        this.sampleData2 = cmd.getSampleData2();

        AggregateLifecycle.apply(new SampleDomainAggregateEvent( 
                                            cmd.getId()
                                            , this.status.name()
                                            , cmd.getSampleData1()
                                            , cmd.getSampleData2() ));
        return this;
    }
    
    public Object handle(FailSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Aggregate Called][Command Handle] handle[FailSampleDomainAggregateCommand]");

        // Validate the last Aggregate Status by Parameter Command
        if(this.status != SampleDomainStatusVO.PLACED) throw new InvalidStatusForCommandException(cmd, this.status);

        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.FAIL;
        this.sampleData1 = cmd.getSampleData1();
        this.sampleData2 = cmd.getSampleData2();

        AggregateLifecycle.apply(new SampleDomainAggregateEvent( 
                                            cmd.getId()
                                            , this.status.name()
                                            , cmd.getSampleData1()
                                            , cmd.getSampleData2() ));
        return this;
	}
}