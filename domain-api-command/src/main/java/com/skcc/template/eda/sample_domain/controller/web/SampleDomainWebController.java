package com.skcc.template.eda.sample_domain.controller.web;

import java.util.UUID;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainRequestDTO;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/sample")
public class SampleDomainWebController {

    private final CommandGateway commandGateway;

    @GetMapping("/create")
    public ResponseEntity<Object> generateCreateSampleDomainAggregateCommand(@RequestBody SampleDomainRequestDTO sampleDomainRequestDTO){
        log.debug("[Web Controller Called] generateCreateSampleDomainAggregateCommand");

        /**
         * 1. Sample Data Generate
         */
        {
            String id = UUID.randomUUID().toString();
            sampleDomainRequestDTO = new SampleDomainRequestDTO(
                id
                , "SampleData1-" + id
                , "SampleData1-" + id
                , sampleDomainRequestDTO.getIsExternalError() );
        }

        /**
         * 2. Generate Aggregate Create Command
         */
        commandGateway.send( new CreateSampleDomainAggregateCommand( 
                                    sampleDomainRequestDTO.getId()
                                    , sampleDomainRequestDTO.getSampleData1()
                                    , sampleDomainRequestDTO.getSampleData2() 
                                    , sampleDomainRequestDTO.getIsExternalError() ));

        return new ResponseEntity<>(HttpStatus.OK);
    }

}