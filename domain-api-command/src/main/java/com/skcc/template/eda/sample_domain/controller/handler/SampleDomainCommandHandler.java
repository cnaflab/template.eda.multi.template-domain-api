package com.skcc.template.eda.sample_domain.controller.handler;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.PlaceSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.SuccessSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleDomainApplicationService;

import org.axonframework.commandhandling.CommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SampleDomainCommandHandler {

    @Autowired
    private ISampleDomainApplicationService sampleDomainApplicationService;

    @CommandHandler
    public void handleCommand(CreateSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Command Handler Called] CreateSampleDomainAggregateCommand");

        sampleDomainApplicationService.createAggregate( cmd );
    }

    @CommandHandler
    public void handleCommand(PlaceSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Command Handler Called] PlaceSampleDomainAggregateCommand");

        sampleDomainApplicationService.placeAggregate( cmd );
    }

    @CommandHandler
    public void handleCommand(SuccessSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Command Handler Called] SuccessSampleDomainAggregateCommand");

        sampleDomainApplicationService.successAggregate( cmd );
    }

    @CommandHandler
    public void handleCommand(FailSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Command Handler Called] FailSampleDomainAggregateCommand");

        sampleDomainApplicationService.failAggregate( cmd );
    }
}