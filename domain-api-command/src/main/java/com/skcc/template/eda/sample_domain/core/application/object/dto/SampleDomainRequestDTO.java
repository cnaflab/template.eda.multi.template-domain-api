package com.skcc.template.eda.sample_domain.core.application.object.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SampleDomainRequestDTO {

    private String id;
    private String sampleData1;
    private String sampleData2;
    int isExternalError;
}